<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<body>
<jsp:include page="menu.jsp"/>

<H1> items </H1>


<sec:authorize access="isAuthenticated()">
    <h2>add new</h2><x></x>
    <form  action="${pageContext.request.contextPath}/admin/items/add" method="post" enctype="multipart/form-data">
        <input type="text" name="name" placeholder="name">
        <br><br>
        <input type="text" name="description" placeholder="description">
        <br><br>
        <input type="file" name="file" placeholder="image">
        <br><br>
        <BUTTON class="btn btn-primary">ADD</BUTTON>
    </form>
</sec:authorize>


<c:forEach var="item" items="${items.content}">


    <h3>name: ${item.name}</h3>
    <p>Id: ${item.id}</p>

    <div>
        description: ${item.description}
    </div>
    <sec:authorize access="isAuthenticated()">
        <div>
            <a href="/admin/items/remove/${item.id}">remove</a>
        </div>
    </sec:authorize>


    <div>
        <a href="/items/${item.id}"> personal page: </a>
    </div>

    <br><br>

</c:forEach>

<div class="navigation">

    <h2>navigation</h2>

    <c:if test="${items.number > 0 }">
        <a href="/items?page=${items.number-1}">prev</a>
    </c:if>

    <p>current: ${items.number}</p>

    <c:if test="${items.number < items.totalPages - 1 }">
        <a href="/items?page=${items.number+1}"> next</a>
    </c:if>
</div>

</body>
<head>
    <jsp:include page="inport_header.jsp"/>
    <link rel="stylesheet" type="text/css" href="/static/css/bootstrap.min.css">

</head>
</html>