<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <jsp:include page="inport_header.jsp"/>

</head>
<body>
<jsp:include page="menu.jsp"/>

<c:if test="${error != null}">
    <p style="color: red"> invalid login or password</p>
</c:if>

<h1> LOGIN PAGE</h1>

    <form method="post" action="/login">
          <br><br>
                    <td>Login:</td>
                    <td><input type='text' name='username' value=''></td>
               <br><br>
                    <td>Password:</td>
                    <td><input type='password' name='password' /></td>
                <br><br>
                    <input type="checkbox" name="remember_me">

                    <br><br>
                    remember me
                    <td><input name="submit" type="submit" value="submit" /></td>

    </form>

</body>
</html>