<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <jsp:include page="inport_header.jsp"/>

</head>
<body>
<jsp:include page="menu.jsp"/>

    <div>
        <h1>
            add admin
        </h1>

        <form method="post" action="/admin/add">
            <input type="text" name="login" placeholder="login">
            <br><br>
            <input type="text" name="password" placeholder="password">
            <br><br>
            <button>add</button>
        </form>

        <div>
            <c:forEach var="admin" items="${admins}">
                <p> login ${admin.login}</p>
                <a href="/admin/remove/${admin.id}"> remove</a>
            </c:forEach>
        </div>
    </div>

</body>
</html>
