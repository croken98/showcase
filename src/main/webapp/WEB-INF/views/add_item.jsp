<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: User-PC
  Date: 13.12.2019
  Time: 22:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <jsp:include page="inport_header.jsp"/>

</head>
<body>
<jsp:include page="menu.jsp"/>


<sec:authorize access="isAuthenticated()">
    <h2>add new</h2><x></x>
    <form  action="${pageContext.request.contextPath}/admin/items/add" method="post" enctype="multipart/form-data">
        <input type="text" name="name" placeholder="name">
        <br><br>
        <input type="text" name="description" placeholder="description">
        <br><br>
        <input type="file" name="file" placeholder="image">
        <br><br>
        <br><br>
        <BUTTON class="btn btn-primary">ADD</BUTTON>

    </form>
</sec:authorize>




</body>
</html>
