<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <jsp:include page="inport_header.jsp"/>

</head>
<body>
<jsp:include page="menu.jsp"/>

<H1> item </H1>

    <sec:authorize access="isAuthenticated()">
        <a href="${pageContext.request.contextPath}/logout">
            <h2>Logout</h2>
        </a>
    </sec:authorize>




<sec:authorize access="isAuthenticated()">
    <h2>Update</h2>

    <form action="/admin/items/update/${item.id}" method="post" enctype="multipart/form-data">
        <input type="text" name="name" value="${item.name}">
        <br><br>
        <input type="text" name="description" value="${item.description}">
        <br><br>
        <input type="file" name="file" placeholder="image">
        <br><br>
        <BUTTON>update</BUTTON>
    </form>
</sec:authorize>



<sec:authorize access="isAuthenticated()">
    <h2>add new picute</h2><x></x>
    <form  action="${pageContext.request.contextPath}/admin/items/add/${item.id}" method="post" enctype="multipart/form-data">
        <input type="file" name="pictures" placeholder="image" multiple>
        <br><br>
        <BUTTON class="btn btn-primary">ADD Picture</BUTTON>

    </form>


<c:forEach var="picturePath" items="${item.picturePaths}">
    <img src="${pageContext.request.contextPath}/static/pictures/${picturePath.picName}">
    <a href="/admin/picture/remove/${picturePath.id}">remove pic</a>
</c:forEach>
</sec:authorize>


    <h3>${item.name}</h3>
    <p>Id: ${item.id}</p>

    <div>
        description: ${item.description}
    </div>
    <div>
        <img src="/static/items/${item.id}.jpg">
    </div>

    <div>
        date of update: ${item.date}
    </div>
    <sec:authorize access="isAuthenticated()">
        <div>
            <a href="/admin/items/remove/${item.id}" >remove</a>
        </div>
    </sec:authorize>


    <br><br>



</body>
</html>