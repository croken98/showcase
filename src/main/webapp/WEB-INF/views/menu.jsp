<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: User-PC
  Date: 07.12.2019
  Time: 23:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav>
    <a href="${pageContext.request.contextPath}/items">all items</a>
    <br>
    <a href="/">index</a>
    <br>

    <sec:authorize access="isAuthenticated()">
        <a href="${pageContext.request.contextPath}/admin/admins">Admins console</a>
        <br>
        <a href="${pageContext.request.contextPath}/logout">
            Logout
        </a>
    </sec:authorize>
</nav>