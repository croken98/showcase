package by.fsc.showcase.beans;


import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Entity(name = "items")
public class Item {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column
    private String name;

    @Column(length = 3000)
    private String description;

    @Column
    private Date date;

    @OneToMany(mappedBy="item", cascade = CascadeType.ALL)
    private List<PicturePath> picturePaths = new ArrayList<>();

}
